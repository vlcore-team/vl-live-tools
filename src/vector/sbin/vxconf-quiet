#!/bin/sh
# Copyright 2001 BSDi, Inc, Concord, CA.  Written by volkerdi@slackware.com.
# Copyright 2004 Slackware Linux, Inc., Concord, CA.
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Modified by rs Lange of Vector Linux. Formally called xorgsetup
# Modified by Uel Archuletta of Vector Linux. Formally called vxconf

# If we aren't root, bail:
if [ "$UID" = "0" ]; then
  TMP=/var/log/setup/tmp
else
clear
  echo "Only root can configure X......bailing"
  sleep 8
  exit
fi
XORG=`which Xorg`

# Now, this wouldn't make much sense either:
if [ ! -x $XORG ]; then
echo "No X found so bailing............"
sleep 8
  exit
fi

if [ ! -d $TMP ]; then
 mkdir -p $TMP
 chmod 700 $TMP
fi

# OK, we'll warn the user if there's already an existing xorg.conf:
CONFIG_EXISTS=false
for xf86config in /etc/X11/xorg.conf /etc/xorg.conf /usr/X11R6/lib/X11/xorg.conf $HOME/xorg.conf ; do
  if [ -r $xf86config ]; then
    CONFIG_EXISTS=$xf86config
  fi  
done

# Move any existing config file(s) aside:
for xf86config in /etc/X11/xorg.conf /etc/xorg.conf /usr/X11R6/lib/X11/xorg.conf $HOME/xorg.conf ; do
  if [ -r $xf86config ]; then
    mv $xf86config ${xf86config}.backup
  fi  
done
##We may need horiz and vert refresh rates later so lets get them now
ddcxinfo -hsync > $TMP/hsync
ddcxinfo -vsync > $TMP/vsync
vert=`cat $TMP/vsync`
horiz=`cat $TMP/hsync`
if [ $vert = "0-0" ]; then
vert="40-90"
fi
if [ $horiz = "0-0" ]; then
horiz="31.5-64"
fi

depth=24
sleep 1
if [ "$1" = "640" ]; then
  finalres="Modes \"640x480\""
elif [ "$1" = "1280" ]; then
  finalres="Modes \"1280x1024\" \"1024x768\" \"800x600\" \"640x480\""
else
  finalres="Modes \"1024x768\" \"800x600\" \"640x480\""
fi

# Have the X server create a default config file:
$XORG -configure :2 2> /dev/null
if [ ! $? = 0 ]; then
  # failure, bail.
  exit
fi

#sleep 1

# I know this completely hoses the indentation of the xorg.conf file, but
# really don't know a good way around that.  Shoulda used perl.  ;)
START_LOOKING=false
rm -f /etc/X11/xorg.conf
cat $HOME/xorg.conf.new | grep -w " HorizSync" 1> /dev/null
if [ $? = 0 ]; then
echo "no" > $TMP/fixsync
else
echo "yes" > $TMP/fixsync
fi
fixsync=`cat $TMP/fixsync`
cat $HOME/xorg.conf.new | while read LINE ; do
   echo "$LINE" >> /etc/X11/xorg.conf
   if echo $LINE | grep Section | grep Screen 1> /dev/null ; then
    START_LOOKING=true
  fi
  if [ "$START_LOOKING" = "true" ]; then
    if echo $LINE | grep Monitor 1> /dev/null ; then
      if [ ! "$depth" = "none" ]; then
        echo "DefaultDepth $depth" >> /etc/X11/xorg.conf
  echo Option         \"RenderAccel\" \"true\"            >> /etc/X11/xorg.conf
  echo Option         \"AllowGLXWithComposite\" \"true\"  >> /etc/X11/xorg.conf
  echo Option         \"AddARGBGLXVisuals\" \"true\"      >> /etc/X11/xorg.conf
  echo Option         \"DisableGLXRootClipping\" \"True\" >> /etc/X11/xorg.conf
      fi
         START_LOOKING=false
    fi
  fi

 if [ ! "$1" = "max" ]; then
  ##lets try a default resolution
  if [ ! "$res" = "none" ]; then 
    if echo $LINE | grep ^Depth 1> /dev/null ; then
	echo $finalres >> /etc/X11/xorg.conf
    fi
  fi
 fi
##now the wheel for a mouse
#if echo $LINE | grep Device | grep mouse 1> /dev/null ; then
#	echo "Option  \"ZAxisMapping\" \"4 5\"" >> /etc/X11/xorg.conf
#	fi	

##now keyboard setting if none us
 if [ ! $XKBMAP ]; then
   XKBMAP=us
 fi
 if [ $XKBMAP ]; then
  if echo $LINE | grep Driver | grep kbd 1> /dev/null ; then
    echo "Option  \"XkbLayout\"  \"$XKBMAP\"   ## KEYBOARD_MAP!" >> /etc/X11/xorg.conf
  fi
 fi
 if [ $XKBMODEL ]; then
  if echo $LINE | grep Driver | grep kbd 1> /dev/null ; then
    echo "Option  \"XkbModel\"  \"$XKBMODEL\"" >> /etc/X11/xorg.conf
  fi
 fi
 if [ $XKBVAR ]; then
  if echo $LINE | grep Driver | grep kbd 1> /dev/null ; then
    echo "Option  \"Xkbvariant\"  \"$XKBVAR\"" >> /etc/X11/xorg.conf
  fi
 fi

##last monitor refresh if not auto detected
if [ "$fixsync" = "yes" ]; then
if echo $LINE | grep ModelName 1> /dev/null ; then
echo "HorizSync $horiz" >> /etc/X11/xorg.conf
fi
fi
if [ "$fixsync" = "yes" ]; then
if echo $LINE | grep ModelName 1> /dev/null ; then
echo "VertRefresh $vert" >> /etc/X11/xorg.conf
fi
fi				
 ## add cyrillic fonts to font path
 if echo $LINE | grep 100dpi 1> /dev/null ; then
   echo "FontPath     \"/usr/lib/X11/fonts/truetype/\"
FontPath     \"/usr/lib/X11/fonts/local/\"
FontPath     \"/usr/lib/X11/fonts/TTF/\"
FontPath     \"/usr/lib/X11/fonts/cyrillic/\" " >> /etc/X11/xorg.conf
 fi
done

echo Section \"DRI\" >> /etc/X11/xorg.conf
echo 	Mode         0666 >> /etc/X11/xorg.conf
echo EndSection >> /etc/X11/xorg.conf

echo " "        >> /etc/X11/xorg.conf
echo Section \"Extensions\" >> /etc/X11/xorg.conf
echo 	Option      \"Composite\"   \"enable\" >> /etc/X11/xorg.conf
echo EndSection >> /etc/X11/xorg.conf

rm -f $TMP/colordepth
rm -f $TMP/resolution
rm -f $TMP/vsync
rm -f $TMP/hsync
rm -f $HOME/xorg.conf.new
clear

### intel i810 video has a problem without this hack
cat /etc/X11/xorg.conf | grep Driver | grep i810 1> /dev/null

if [ $? = 0 ]; then
  uname -r | cut -f 1,2 -d '.' > $TMP/kernel
  kernel=`cat $TMP/kernel`
  rm -f $TMP/kernel
  if [ $kernel = "2.6" ]; then
   /sbin/modprobe intel-agp && echo "/sbin/modprobe intel-agp" >> /etc/rc.d/rc.modules
  fi
fi

/bin/grep ^Driver /etc/X11/xorg.conf | grep "nv"
if [ $? == 0 ]; then
# if [ -e /lib/modules/`uname -r`/kernel/drivers/video/nvidia.ko ]; then
#   echo module exists
# else
  if [ -e /etc/nvidia*.tlz ];then  
    echo installing package
    installpkg /etc/nvidia*.tlz 1>/dev/null #2>/dev/null
  fi
  if [ -e /lib/modules/`uname -r`/kernel/drivers/video/nvidia.ko ]; then
  NV=`/bin/grep ^Driver /etc/X11/xorg.conf | grep "nv"|cut -d \" -f 2`
    if [ "$NV" == "nv" ];then
      sed s'/nv/nvidia/g' /etc/X11/xorg.conf >/etc/X11/xorg.conf.new
      mv /etc/X11/xorg.conf.new /etc/X11/xorg.conf
    fi
  fi
# fi
else
 echo $?
fi
/etc/rc.d/rc.text 20 'Testing X configuration...........hold tight!!!!'
echo "Testing X configuration...........hold tight!!!!"
sleep 2
$XORG -probeonly :2 2> /dev/null
if [ $? = 0 ]; then
/etc/rc.d/rc.text 20 'X was configured succesfully'
 echo " X was configured succesfully"
else
# splash -s -u 0 /etc/bootsplash/themes/VectorBlue-3/config/verbose-1024x768.cfg
echo "There was an error in your X configuration. Please edit the \
 /etc/X11/xorg.conf file as autoconfig must have stumbled somewhere! \
 Sorry but this happens sometimes." >> /dev/tty0
 fi
 
